﻿/////////////////////////////////////////////////////////////////////////////
// Script by Matt D ///////////////////////////////////////////////////////////
// This script will scan the selected story for any tables. ////////////////////////////
// For each table it will set it's width to match the parent text frame column width //////
// and evenly distribute the table column widths. ///////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

main();

function main(){
	var myObject;
	//Make certain that user interaction (display of dialogs, etc.) is turned on.
	app.scriptPreferences.userInteractionLevel = UserInteractionLevels.interactWithAll;
	if(app.documents.length > 0){
		if(app.selection.length > 0){
			switch(app.selection[0].constructor.name){
				case "InsertionPoint":
				case "Character":
				case "Word":
				case "TextStyleRange":
				case "Line":
				case "Paragraph":
				case "TextColumn":
				case "Text":
				case "Cell":
				case "Column":
				case "Row":
				case "Table":
					myDisplayDialog();
					break;
				default:
					//Something was selected, but it wasn't a text object, so search the document.
					myFindChangeByList(app.documents.item(0));
			}
		}
		else{
			//Nothing was selected, so simply search the document.
			myFindChangeByList(app.documents.item(0));
		}
	}
	else{
		alert("No documents are open. Please open a document and try again.");
	}
}
function myDisplayDialog()
{
	var myObject;
	var myDialog = app.dialogs.add({name:"FindChangeByList"});
	with(myDialog.dialogColumns.add()){
		with(dialogRows.add()){
			with(dialogColumns.add()){
				staticTexts.add({staticLabel:"Search Range:"});
			}
			var myRangeButtons = radiobuttonGroups.add();
			with(myRangeButtons){
				radiobuttonControls.add({staticLabel:"Document", checkedState:true});
				radiobuttonControls.add({staticLabel:"Selected Story"});
				if(app.selection[0].contents != ""){
					radiobuttonControls.add({staticLabel:"Selection", checkedState:true});
				}
			}			
		}
	}
	var myResult = myDialog.show();
	if(myResult == true){
		switch(myRangeButtons.selectedButton){
			case 0:
				myObject = app.documents.item(0).stories.everyItem();
				break;
			case 1:
				myObject = app.selection[0].parentStory;
				break;
			case 2:
				myObject = app.selection[0];
				break;
		}
		myDialog.destroy();
		findTables(myObject);
         //convertHeadings(myObject);
         //styleOutro(myObject);
         removeUnusedColors();
         nonBoldHeadings();
	}
	else{
		myDialog.destroy();
	}
}

function findTables(myObject)
{
    allTablesInStory = myObject.tables.everyItem().getElements();
    //check if the story contains any tables.
    if(allTablesInStory.length > 0){
            //loop through the array of tables
        for (table=0; table<allTablesInStory.length; table++)
        {
            processTable (allTablesInStory[table]);
        }
    }
    return allTablesInStory;
}


function processTable(table)
{
    //calculate what the table column width needs to be to distributed evenly using the parent text frame column width.
    textFrame = table.parent;
    textFrameColAmount = textFrame.textFramePreferences.textColumnCount;
    textFrameMultipleCols = textFrameColAmount - 1;  //The cols. after the first one
    textFrameBounds =  textFrame.geometricBounds;
    textFrameGutterWidth = textFrame.textFramePreferences.textColumnGutter;
    textFrameColWidth = ( (textFrameBounds[3] - textFrameBounds[1]) - (textFrameGutterWidth * textFrameMultipleCols) ) / textFrameColAmount;
    tableColAmount = table.columnCount;
    tableColWidth = textFrameColWidth / tableColAmount;
    //set the calculated width for each cell in the table.
    table.cells.everyItem().width = tableColWidth;
    //add extra styling if table is a comment table.
    if (table.appliedTableStyle.name == "Comment Table"){
        if (table.rows.item(0).rowType != RowTypes.headerRow){
            table.rows.item(0).rowType = RowTypes.headerRow;
        }
        table.rows.everyItem().cells.everyItem().clearCellStyleOverrides();
    }
    else if (table.appliedTableStyle.name == "Rates Table"){
        // Convert the first row to a header row
        if (table.rows.item(0).rowType != RowTypes.headerRow){
            table.rows.item(0).rowType = RowTypes.headerRow;
        }
        table.rows.everyItem().cells.everyItem().clearCellStyleOverrides();
    }
    else {
        table.rows.everyItem().cells.everyItem().clearCellStyleOverrides();
    }
}

function nonBoldHeadings()
{
    app.findTextPreferences = app.changeTextPreferences = null;  
    app.findTextPreferences.appliedParagraphStyle = "Title" //won't work if it's in a style group !!!
    var headings = app.activeDocument.selection[0].parentStory.findText(true); //make sure you are inside a text frame when running the script  
    for (var i = 0; i < headings.length; i++) {  
        var heading = headings[i]
        if (heading.appliedCharacterStyle.name == "Bold"){
            ApplyNone(heading);
        }
        else if (heading.appliedCharacterStyle.name == "Italic"){
            ApplyNone(heading);
        }
    }
    app.findTextPreferences = app.changeTextPreferences = null;  
    app.findTextPreferences.appliedParagraphStyle = "Subtitle" //won't work if it's in a style group !!!
    var headings = app.activeDocument.selection[0].parentStory.findText(true); //make sure you are inside a text frame when running the script  
    for (var i = 0; i < headings.length; i++) {  
        var heading = headings[i]
        if (heading.appliedCharacterStyle.name == "Bold"){
            ApplyNone(heading);
        }
        else if (heading.appliedCharacterStyle.name == "Italic"){
            ApplyNone(heading);
        }
    }
    app.findTextPreferences = app.changeTextPreferences = null;  
    app.findTextPreferences.appliedParagraphStyle = "Heading1" //won't work if it's in a style group !!!
    var headings = app.activeDocument.selection[0].parentStory.findText(true); //make sure you are inside a text frame when running the script  
    for (var i = 0; i < headings.length; i++) {  
        var heading = headings[i]
        if (heading.appliedCharacterStyle.name == "Bold"){
            ApplyNone(heading);
        }
        else if (heading.appliedCharacterStyle.name == "Italic"){
            ApplyNone(heading);
        }
    }
    app.findTextPreferences = app.changeTextPreferences = null;  
    app.findTextPreferences.appliedParagraphStyle = "Heading2" //won't work if it's in a style group !!!
    var headings = app.activeDocument.selection[0].parentStory.findText(true); //make sure you are inside a text frame when running the script  
    for (var i = 0; i < headings.length; i++) {  
        var heading = headings[i]
        if (heading.appliedCharacterStyle.name == "Bold"){
            ApplyNone(heading);
        }
        else if (heading.appliedCharacterStyle.name == "Italic"){
            ApplyNone(heading);
        }
    }
    app.findTextPreferences = app.changeTextPreferences = null;  
    app.findTextPreferences.appliedParagraphStyle = "Heading3" //won't work if it's in a style group !!!
    var headings = app.activeDocument.selection[0].parentStory.findText(true); //make sure you are inside a text frame when running the script  
    for (var i = 0; i < headings.length; i++) {  
        var heading = headings[i]
        if (heading.appliedCharacterStyle.name == "Bold"){
            ApplyNone(heading);
        }
        else if (heading.appliedCharacterStyle.name == "Italic"){
            ApplyNone(heading);
        }
    }
    app.findTextPreferences = app.changeTextPreferences = null;  
    app.findTextPreferences.appliedParagraphStyle = "Heading4" //won't work if it's in a style group !!!
    var headings = app.activeDocument.selection[0].parentStory.findText(true); //make sure you are inside a text frame when running the script  
    for (var i = 0; i < headings.length; i++) {  
        var heading = headings[i]
        if (heading.appliedCharacterStyle.name == "Bold"){
            ApplyNone(heading);
        }
        else if (heading.appliedCharacterStyle.name == "Italic"){
            ApplyNone(heading);
        }
    }
}

function ApplyNone(heading) {
    var noneStyle = app.activeDocument.characterStyles.item("[None]");  
    /*if (app.activeDocument.characterStyles.item("None") == null) {
        var basedOnNone = app.activeDocument.characterStyles.add({name:"None", basedOn:noneStyle});  
    }*/
     heading.appliedCharacterStyle = app.activeDocument.allCharacterStyles[0];
     heading.clearOverrides();  
     //basedOnNone.remove(noneStyle); 
} 

/*function convertHeadings(myObject)
{
    app.findTextPreferences = app.changeTextPreferences = null;  
    app.findTextPreferences.appliedParagraphStyle = "Heading1" //won't work if it's in a style group !!!
    var myF = app.activeDocument.selection[0].parentStory.findText(true); //make sure you are inside a text frame when running the script  
    for (var i = 0; i < myF.length; i++) {  
        var myT = myF[i].convertToTable("#", "#", 1);  
        myT.appliedTableStyle = "Heading1";  
        myT.clearTableStyleOverrides(); //this line might need to be commented out, see how it works with or without it 
        //myT.rows.everyItem().cells.everyItem().paragraph.everyItem().appliedCharacterStyle = "White";
        myT.rows.everyItem().cells.everyItem().paragraphs.everyItem().appliedCharacterStyle = "White";
        myT.rows.everyItem().cells.everyItem().appliedCellStyle = "Heading1";
        myT.rows.everyItem().cells.everyItem().clearCellStyleOverrides();
        //remove the empty paragraphs that are left after the conversion.
        heading1Paras = myT.rows.everyItem().cells.everyItem().paragraphs.everyItem();
        // clear grep prefs
        app.findGrepPreferences = app.changeGrepPreferences = null;
        // Remove empty paragraphs and excess space at the end of paragraphs
        app.findGrepPreferences.findWhat = "\r";
        app.changeGrepPreferences.changeTo = "";
        heading1Paras.changeGrep();
        // clear grep prefs
        app.findGrepPreferences = app.changeGrepPreferences = null;
    }  
}*/

/*function styleOutro(myObject)
{
    numberOfParasInStory = (app.selection[0].parentStory.paragraphs.length);
    lastParaIndex = (numberOfParasInStory -1)
    lastPara = app.selection[0].parentStory.paragraphs[lastParaIndex];
    outroStyle = "Outro";
    lastPara.appliedParagraphStyle = "Outro";
}*/

function removeUnusedColors()
{
    var myIndesignDoc = app.activeDocument;
    var myUnusedSwatches = myIndesignDoc.unusedSwatches;
    for (var s = myUnusedSwatches.length-1; s >= 0; s--) {
        var mySwatch = myIndesignDoc.unusedSwatches[s];
        var name = mySwatch.name;
        // alert (name);
        if (name != ""){
            mySwatch.remove();
        }
    }
}

/*function addHeaderRow(myObject, allTablesInStory)
{*/
    /*for (table=0; table<allTablesInStory.length; table++)
    {
        if (table.appliedTableStyle = "Comment Table"){
            // Convert the first row to a header row
            table.rows.item(0).rowType = RowTypes.headerRow;
            //table.cells.everyItem().paragraphs.appliedCharacterStyle = "White";
        }
        else {
            alert("No Comment tables found")
        }
    }*/
/*
    var doc = app.activeDocument;
    var story = doc.stories[0];
    // Get every paragraph in `story` (using `everyItem().getElements()` is more efficient)
    var tables = story.tables.everyItem().getElements();
    // Collect every paragraph with a certain paragraph style
    var applied_style = doc.tableStyles.itemByName('CommentTable');
    var tables_with_style = [];
    for (var i=0,l=tables.length; i<l; i++) {
       var table = tables[i];
       if (table.appliedParagraphStyle == applied_style) {
          tables_with_style.push(table);
       }
    }
    // Do something with each `para` in `paras_with_style`
    alert(tables_with_style);
}*/

// Get every paragraph in `story` (using `everyItem().getElements()` is more efficient)
////// var paras = story.paragraphs.everyItem().getElements();


//// SAVE manipulate objects if they contain certain text
/*
    var myDocument:Document = app.documents.item(0);
var myStory:Story = myDocument.stories.item(0);
//This loop formats all of the paragraphs by iterating 
//backwards through the paragraphs in the story.
for(var myParagraphCounter:int = myStory.paragraphs.length-1; myParagraphCounter >= 0; myParagraphCounter --)
{
   if(myStory.paragraphs.item(myParagraphCounter).words.item(0).contents=="Delete"){
      myStory.paragraphs.item(myParagraphCounter).remove();
   }
   else{
      myStory.paragraphs.item(myParagraphCounter).pointSize = 24;
   }
}
*/
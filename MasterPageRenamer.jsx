﻿/////////////////////////////////////////////////////////////////////////////
// Title: Odd Even Master Pages
// Version: 0.1
// Author: Matt Dawson, matt.dawson@mercia-group.co.uk
// Description: This script loops through all pages in the document,
// and applies 'B-Master' master page to even pages and 'A-Master' to odd pages.
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
// -- Global Variables -- //

// Current document
var doc = app.documents[0];
var docName = doc.name;
// List of master pages
var docMasters = doc.masterSpreads.everyItem().name;
//alert(docMasters);
var pagesLength = doc.pages.length;

var isEven = function(someNumber){
    return (someNumber%2 == 0) ? true : false;
};

/////////////////////////////////////////////////////////////////////////////
// -- Functions -- //

function masterSelect() {

    with (theDialog = app.dialogs.add({ name: 'Select a Master for each page type' })) {

        //Add a dialog column.
        DialogColumn1 = dialogColumns.add();
        with (DialogColumn1) {
            staticTexts.add({ staticLabel: 'Odd Master:' });
            staticTexts.add({ staticLabel: 'Even Master:' });
        }

        DialogColumn2 = dialogColumns.add();
        with (DialogColumn2) {
            oddMaster = dropdowns.add({ stringList: docMasters, selectedIndex: 0 });
            evenMaster = dropdowns.add({ stringList: docMasters, selectedIndex: 0 });
        }

        if (theDialog.show()) {
            var results = new Object();
            results['oddMasterName'] = oddMaster.stringList[oddMaster.selectedIndex];
            results['evenMasterName'] = evenMaster.stringList[evenMaster.selectedIndex];
            //alert(results['oddMasterName']);
            //alert(results['evenMasterName']);
            theDialog.destroy;
            return (results);
        }
        else {
            theDialog.destroy();
            return (false);
        }
    }
}

function applyMasters(userChoices){
    // Loop through all pages...
    for ( var i = 0; i < pagesLength; i++ ) {

        var pageNumber = i+1;

        // Currently active page
        var active_page = doc.pages[ i ];

        //alert('Page number: '+pageNumber);

        if(isEven(pageNumber) == true) {
            //Apply Master B for right page
            //alert('Apply B-Master');
            active_page.appliedMaster = app.activeDocument.masterSpreads.item(userChoices['evenMasterName']);
        }
        else {
            //Apply Master A for left page
            //alert('Apply A-Master');
            active_page.appliedMaster = app.activeDocument.masterSpreads.item(userChoices['oddMasterName']);
        }

    }
}

/////////////////////////////////////////////////////////////////////////////
// -- App start -- //

var userChoices = masterSelect();
//alert(userChoices['oddMasterName']);
//alert(userChoices['evenMasterName']);
applyMasters(userChoices);
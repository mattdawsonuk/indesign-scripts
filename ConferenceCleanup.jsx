﻿/////////////////////////////////////////////////////////////////////////////
// Script by Matt D /////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

// var doc = app.activeDocument;  
// var mRectangle = doc.xmlElements[0].xmlElements.itemByName("IMAGE").xmlContent.parent;  
// var mBounds = mRectangle.geometricBounds;  
// mRectangle.geometricBounds= [mBounds[0], mBounds[1], mBounds[2], mBounds[1]+160];  
// mRectangle.graphics[0].fit(FitOptions.proportionally);  
// mRectangle.graphics[0].fit(FitOptions.centerContent);

// Xmltag2objectstyle.jsx
// applies an object style to xmlelements with
// the tag thetagname.
// Made by Teus de Jong, last version August 5, 2005

// var themarkuptag, objectstylename;

// doDisplayDialog();

// if (!doDisplayDialog()) { exit(); }
// theobjectstyle = app.activeDocument.objectStyles.item(objectstylename);
// if (theobjectstyle == null) {

//     alert('The object style ' + objectstylename + ' does not exist!');
//     exit();
// }
// travermlelements(app.activeDocument);

// function travermlelements(elm) {

//     var i;
//     for (i = 0; i < elm.xmlElements.length; i++) {

//         if (elm.xmlElements[i].markupTag.name == themarkuptag) {

//             dothething(elm.xmlElements[i]);
//         }
//         travermlelements(elm.xmlElements[i]);
//     }
// }

// function dothething(elm) {
//     var obj = null;
//     if (elm.pageItems.length != 0) {

//         obj = elm.pageItems[0];
//     } else if (elm.images.length != 0) {

//         obj = elm.images[0].parent;
//     } else if (elm.epss.length != 0) {

//         obj = elm.epss[0].parent;
//     } else if (elm.pdfs.length != 0) {

//         obj = elm.pdfs[0].parent;
//     }
//     if (obj != null) {

//         obj.applyObjectStyle(theobjectstyle, true);
//     }
// }
// function doDisplayDialog() {
//     with (theDialog = app.dialogs.add({ name: 'Select xml tag and Object style' })) {

//         //Add a dialog column.
//         DialogColumn1 = dialogColumns.add();
//         with (DialogColumn1) {

//             staticTexts.add({ staticLabel: 'markup Tag:' });
//             staticTexts.add({ staticLabel: 'Object style:' });
//         }
//         DialogColumn2 = dialogColumns.add();
//         with (DialogColumn2) {

//             xmltagbox = dropdowns.add({ stringList: app.activeDocument.xmlTags.everyItem().name, selectedIndex: 0 });
//             objectstylebox = dropdowns.add({ stringList: app.activeDocument.objectStyles.everyItem().name, selectedIndex: 0 });
//         }
//         if (theDialog.show()) {

//             themarkuptag = xmltagbox.stringList[xmltagbox.selectedIndex];;
//             objectstylename = objectstylebox.stringList[objectstylebox.selectedIndex];;
//             theDialog.destroy;
//             return (true);
//         }
//         else {

//             theDialog.destroy();
//             return (false);
//         }
//     }
// }

themarkuptag = 'photo';
objectstylename = 'SpeakerPhoto';
theobjectstyle = app.activeDocument.objectStyles.item(objectstylename);
if (theobjectstyle == null) {

    alert('The object style ' + objectstylename + ' does not exist!');
    exit();
}
travermlelements(app.activeDocument);

function travermlelements(elm) {

    var i;
    for (i = 0; i < elm.xmlElements.length; i++) {

        if (elm.xmlElements[i].markupTag.name == themarkuptag) {

            dothething(elm.xmlElements[i]);
        }
        travermlelements(elm.xmlElements[i]);
    }
}

// function dothething(elm){

// var obj = null;
// if (elm.pageItems.length != 0){

// obj = elm.pageItems[0];
// } else if (elm.images.length !=0 ){

// obj = elm.images[0];
// } else if (elm.epss.length != 0){

// obj = elm.epss[0];
// } else if (elm.pdfs.length !=0){

// obj = elm.pdfs[0]
// }
// if (obj != null){

// obj.applyObjectStyle(theobjectstyle, true);
// }
// }

function dothething(elm) {

    var obj = null;
    if (elm.pageItems.length != 0) {

        obj = elm.pageItems[0];
    } else if (elm.images.length != 0) {

        obj = elm.images[0].parent;
    } else if (elm.epss.length != 0) {

        obj = elm.epss[0].parent;
    } else if (elm.pdfs.length != 0) {

        obj = elm.pdfs[0].parent;
    }
    if (obj != null) {
        obj.applyObjectStyle(theobjectstyle, true);
        var mBounds = obj.geometricBounds;  
        obj.geometricBounds= [mBounds[0], mBounds[1], mBounds[0]+16, mBounds[1]+16];  
        obj.graphics[0].fit(FitOptions.FILL_PROPORTIONALLY);  
        obj.graphics[0].fit(FitOptions.centerContent);
    }
}